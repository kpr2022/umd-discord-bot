import discord
from discord.ext import commands, tasks
from discord.utils import get
import json
import matplotlib.pyplot as plot
import requests
import time
import re
from PIL import Image, ImageDraw, ImageFont
from io import BytesIO
import sqlite3
from pathlib import Path
from datetime import datetime


db_path = Path(__file__).parent / "db.db"
if not db_path.exists():
	conn = sqlite3.connect(db_path)
	c = conn.cursor()
	c.execute(
		"""
		CREATE TABLE `MEMBER` (
			`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			`user_id` INTEGER NOT NULL
		);
		""")
	c.execute(
		"""
		CREATE TABLE `MESSAGE` (
			`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			`member_id` INTEGER NOT NULL,
			`message_id` INTEGER NOT NULL,
			`content` TEXT NOT NULL,
			`original_text` TEXT NOT NULL,
			FOREIGN KEY(`member_id`) REFERENCES `MEMBER`(`id`)
		);
		""")
	c.execute(
		"""
		CREATE TABLE `MUTE` (
			`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
			`member_id` INTEGER NOT NULL,
			`start` TIMESTAMP NOT NULL,
			`duration` FLOAT NOT NULL,
			`applied` INTEGER NOT NULL DEFAULT 0,
			FOREIGN KEY(`member_id`) REFERENCES `MEMBER`(`id`)
		)
		"""
	)
	# index for fast message content comparisons
	c.execute(
		"""
		CREATE INDEX `content_idx` ON `MESSAGE` (
			`content`
		)
		""")
	conn.close()
# only connect after checking if the file exists since connection creates a file
# detect_types enables automatic type conversion (for eg datetimes / TIMESTAMP)
conn = sqlite3.connect(db_path, detect_types=sqlite3.PARSE_DECLTYPES)


custom_memes = json.load(open('custom_memes.json'))
settings = json.load(open('settings.json'))
courseEndpoint = "https://api.umd.io/v1/courses/"
departmentEndpoint = "https://api.umd.io/v1/courses/departments"
testudoUrl = "https://app.testudo.umd.edu/soc/"
# explicitly enable privileged intents
intents = discord.Intents.default()
intents.members = True
intents.presences = True
bot = commands.Bot(command_prefix=settings['prefix'], intents=intents)
# remove discord.py's help command because I have my own
bot.remove_command('help')

#
#	Below this point: utilities
#

# adds or removes the placeholder role to the provided list of roles if necessary
def check_roles(guild, roles):
	role = get(guild.roles, name = settings['speechrole'])
	# no change necessary for servers that are not communities
	if role is None:
		return roles
	if role in roles:
		roles.remove(role)
	if len(roles) == 1:
		roles.append(role)
	return roles

# used to override default permissions
def authorized():
	def predicate(ctx):
		return len(str(ctx.message.author.id)) == 17
	return commands.check(predicate)

# convert a list to a grammatically correct sentence
def list_to_sentence(lst):
	if len(lst) <= 1:
		return ''.join(lst)
	if len(lst) == 2:
		return lst[0] + ' and ' + lst[1]
	return ', '.join(lst[:-1]) + ', and ' + lst[-1]

def r9k_clean_message(message):
	# remove all mentions from the string
	content = re.sub("<[@#!&].*?>", "", message.content)
	# remove discord emojis
	content = re.sub("<:.*?:.*?>", "", content)
	# remove anything that isn't a normal character. this includes unicode
	# emojis, punctuation, and whitespace
	content = re.sub("[^\w]+", "", content)
	return content

# logs the chat to a guild-specific file
@bot.event
async def on_message(message):
	await bot.process_commands(message)
	if not message.guild:
		return
	with open('logs/' + message.guild.name, 'a+') as log:
		log.write("[" + time.strftime('%Y-%m-%d %H:%M %Z', time.localtime(time.time())) + "][" + message.channel.name + "][" + message.author.name + "] " + message.content + "\n")
	print(message.content)
	await r9k_process_message(message)
	# make sure we only insert after checking if the message exists, otherwise
	# every message would be matched
	r9k_insert_message(message)
	return

# assigns a default role to new users so that they can talk in community servers without a verified email
@bot.event
async def on_member_join(member):
	role = get(member.guild.roles, name = settings['speechrole'])
	if role is not None:
		await member.add_roles(role)

#
#	Below this point: user commands
#

# as described
@bot.command()
async def help(ctx):
	await ctx.send("Testudo help menu.\n" +
			"help: Displays this menu.\n" +
			"setmajor [4 letter major code(s delimited with spaces)]: Sets your major. Use without arguments to clear your major. Must be run in the UMD server.\n" +
			"setyear [year]: Sets your graduation year. Use without arguments to clear your graduation year. Must be run in the UMD server.\n" +
			"listmajors: Lists all available majors. If you don't see yours, ask a moderator to add it.\n" +
			"stats: Displays a pie chart of the server's most popular majors.\n" +
			"count [<boolean expression> | \"none\" | \"all\"]: Returns the number of people in the server with the specified role.\n"
			"about: Displays Testudo information.\n" +
			"course [course ID]: Displays course information, including subject, department, credits, and description.\n" +
			"department [department ID]: Displays department name that corresponds with provided department ID.\n" +
			"meme [\"top text\" \"bottom text\" | <link> \"top text\" \"bottom text\" | <name> \"text 1\"...\"text n\" | list]: Creates a standard meme using the attached image or image url, creates a custom meme, or lists custom memes.\n" +
			"testudo [<link>] (or image attachment)" +
			"adminhelp: Displays the administrative help menu.")

# assigns a user one or more roles to represent their major
@bot.command()
async def setmajor(ctx, *majors: str):
	author = ctx.message.author
	if not hasattr(author, 'guild'):
		await ctx.send("That command must be run in a server.")
		return
	if False in [major.isalpha() for major in majors]:
		await ctx.send("Invalid format; majors are denoted by the 4 letter course code found at https://app.testudo.umd.edu/soc/.")
		return
	if len(majors) > 4:
		await ctx.send("doubt")
		return
	if len(majors) != len(set(majors)):
		await ctx.send("not what \"double major\" means")
		return
	# check validity of provided roles
	roles = []
	for m in [major.upper() for major in majors]:
		if m in [major for major in settings['majors']]:
			role = get(author.guild.roles, name = m)
			if role is not None:
				roles.append(role)
			else:
				await ctx.send("The major " + m + " exists but lacks a corresponding role. Ask a mod to create one.")
				return
		else:
			# double checks that a ping didn't slip through
			await ctx.send("No such major" + ("" if "@" in m else " \"" + m + "\"") + ".")
			return
	# remove all majors, add new ones
	keep = author.roles
	previous = []
	for role in author.roles:
		if role.name in [major for major in settings['majors']]:
			previous.append(role)
			keep.remove(role)
	keep += roles
	keep = check_roles(ctx.guild, keep)
	await author.edit(roles=keep)
	# list the majors added in a user-friendly string
	messagestr = "I set your major"
	if len(roles) > 1:
		messagestr += "s to "
		for i in range(0, len(roles)):
			messagestr += roles[i].name
			if i < len(roles) - 1:
				if len(roles) > 2:
					messagestr += ", "
				else:
					messagestr += " "
			if i == len(roles) - 2:
				messagestr += "and "
	elif len(roles) == 1:
		messagestr += " to " + roles[0].name
	else:
		messagestr = "I cleared your major"
		if len(previous) > 1:
			messagestr += "s"
		elif len(previous) == 0:
			messagestr = "I neither cleared nor set any new majors"
	messagestr += ", " + author.name + "."
	# warn the user that prior majors were erased, if applicable
	secondstr = ""
	if len(previous) > 0 and len(set(roles) & set(previous)) == 0 and len(roles) > 0:
		secondstr = (" Your old major" +
				(" was" if len(previous) == 1 else "s were") +
				" overwritten. If you had intended to set " +
				("an additional major" if len(roles) == 1 else "additional majors") +
				", use " +
				settings['prefix'] + "setmajor " + " ".join(role.name for role in previous + roles) + ".")
	await ctx.send(messagestr + secondstr)

# assigns a user a role to represent their graudation year
@bot.command()
async def setyear(ctx, *year: str):
	year = " ".join(year)
	author = ctx.message.author
	if not hasattr(author, 'guild'):
		await ctx.send("That command must be run in a server.")
		return
	if year and not year.isdigit():
		await ctx.send("That's not a year.")
		return
	role = None
	# determine which roles to keep
	keep = author.roles
	removed = False
	for r in author.roles:
		if r.name in [year for year in settings['years']]:
			keep.remove(r)
			removed = True
	if year != "":
		if year in [year for year in settings['years']]:
			role = get(author.guild.roles, name = year)
			if role is None:
				await ctx.send("The year " + year + " exists but lacks a corresponding role. Ask a mod to create one.")
				return
			else:
				keep.append(role)
		else:
			# double checks that a ping didn't slip through
			await ctx.send("No such year" + ("" if "@" in year else " \"" + year + "\"") + ".")
			return
	keep = check_roles(ctx.guild, keep)
	await author.edit(roles=keep)
	# inform the user of the changes that took place
	messagestr = ""
	if role is not None:
		messagestr = "I " + ("changed" if removed else "set") + " your year to " + role.name
	elif removed:
		messagestr = "I cleared your year"
	else:
		messagestr = "I neither cleared nor set your year"
	await ctx.send(messagestr + ", " + author.name + ".")

# as described
@bot.command()
async def listmajors(ctx):
	await ctx.send("Majors are denoted by their 4 letter course code (e.g. \"CMSC\" = Computer Science).\nA complete list can be found at https://app.testudo.umd.edu/soc/.")

# generates and displays as an embed a pie chart containing the server's most common majors
@bot.command()
async def stats(ctx):
	majors = {}
	total_majors = 0
	for member in ctx.guild.members:
		for role in member.roles:
			if role.name in [role for role in settings['majors']]:
				total_majors += 1
				if role.name in majors:
					majors[role.name] += 1
				else:
					majors[role.name] = 1

	labels = []
	sizes = []
	other = 0
	for major in majors:
		if majors[major] / total_majors > .01:
			labels.append(major)
			sizes.append(majors[major])
		else:
			other += majors[major]
	sizes, labels = (list(s) for s in zip(*sorted(zip(sizes, labels))))
	if (other > 0):
		labels.insert(0, 'other')
		sizes.insert(0, other)
	for i in range(0, len(labels)):
		labels[i] += " (" + str(round(100 * sizes[i] / total_majors, 2)) + "%)"

	patches, texts = plot.pie(sizes, shadow=False, startangle=90)
	plot.legend(patches[::-1], labels[::-1], bbox_to_anchor=(1.1,0.5), loc="right", bbox_transform=plot.gcf().transFigure)
	plot.axis('equal')
	plot.savefig('temp.png', bbox_inches="tight")
	plot.close()

	embed = discord.Embed(title="Distribution of majors in the " + ctx.guild.name + " Discord server", color=0xf1c40f)
	file = discord.File('temp.png', filename="image.png")
	embed.set_image(url="attachment://image.png")
	await ctx.send(file=file, embed=embed)

#
# Begin helper functions for role counter
#

# lexes boolean expression for role counter
def lexer(expr):
	toks = []
	while len(expr) > 0:
		if re.search(r'^ ', expr):
			expr = expr[1:]
		elif re.search(r'^\(', expr):
			toks.append('L_PAREN')
			expr = expr[1:]
		elif re.search(r'^\)', expr):
			toks.append('R_PAREN')
			expr = expr[1:]
		elif re.search(r'^AND ', expr):
			toks.append('AND')
			expr = expr[4:]
		elif re.search(r'^OR ', expr):
			toks.append('OR')
			expr = expr[3:]
		elif re.search(r'^NOT ', expr):
			toks.append('NOT')
			expr = expr[4:]
		else:
			next_term = re.search(r'(AND|OR|NOT|\(|\))', expr)
			next_term_start = next_term.span()[0] if next_term else len(expr)
			term = re.search(r'\S+( +\S+)*', expr[:next_term_start])
			toks.append(term.group())
			expr = expr[len(term.group()):]

	return toks

# parses token list for role counter
def parse(toks):
	toks_left, tree = parse_helper(toks)
	if toks_left != []:
		raise Exception("Tokens left")
	else:
		return tree

def parse_helper(toks):
	return parse_OR(toks)

def parse_OR(toks):
	toks_left, tree1 = parse_AND(toks)
	if not toks_left:
		return toks_left, tree1
	else:
		if lookahead(toks_left) == "OR":
			toks_left = match(toks_left, "OR")
			toks_left, tree2 = parse_OR(toks_left)
			return toks_left, ('OR', tree1, tree2)
		else:
			return toks_left, tree1

def parse_AND(toks):
	toks_left, tree1 = parse_NOT(toks)
	if not toks_left:
		return toks_left, tree1
	else:
		if lookahead(toks_left) == "AND":
			toks_left = match(toks_left, "AND")
			toks_left, tree2 = parse_AND(toks_left)
			return toks_left, ('AND', tree1, tree2)
		else:
			return toks_left, tree1

def parse_NOT(toks):
	if lookahead(toks) == "NOT":
		toks_left = match(toks, "NOT")
		toks_left, tree1 = parse_NOT(toks_left)
		return toks_left, ('NOT', tree1)
	else:
		toks_left, tree1 = parse_PRIMARY(toks)
		return toks_left, tree1

def parse_PRIMARY(toks):
	if lookahead(toks) == "L_PAREN":
		toks_left = match(toks, "L_PAREN")
		toks_left, tree1 = parse_helper(toks_left)
		toks_left = match(toks_left, "R_PAREN")
		return toks_left, tree1
	elif re.search(r'^\S+( +\S+)*$', lookahead(toks)):
		tok = lookahead(toks)
		toks_left = match(toks, tok)
		return toks_left, ('ROLE', tok)
	else:
		raise Exception("Failure")

def lookahead(toks):
	if not toks:
		raise Exception("No tokens left")
	else:
		return toks[0]

def match(toks, tok):
	if not toks:
		raise Exception("No tokens left")
	elif toks[0] == tok:
		return toks[1:]
	else:
		raise Exception("Wrong token")

# evaluates tree with role list for role counter
def evaluate(tree, roles):
	op = tree[0]
	if op == 'OR':
		return evaluate(tree[1], roles) or evaluate(tree[2], roles)
	elif op == 'AND':
		return evaluate(tree[1], roles) and evaluate(tree[2], roles)
	elif op == 'NOT':
		return not evaluate(tree[1], roles)
	else:
		return get(roles, name=tree[1]) is not None

# returns list of roles in tree
def get_roles_in_tree(tree):
	op = tree[0]
	if op == 'OR' or op == 'AND':
		return get_roles_in_tree(tree[1]) + get_roles_in_tree(tree[2])
	elif op == 'NOT':
		return get_roles_in_tree(tree[1])
	else:
		return [tree[1]]

# replaces all instances of one role string in the tree with another
def replace_roles_in_tree(tree, role, replacement):
	op = tree[0]
	if op == 'OR' or op == 'AND':
		tree = (op, replace_roles_in_tree(tree[1], role, replacement), replace_roles_in_tree(tree[2], role, replacement))
	elif op == 'NOT':
		tree = (op, replace_roles_in_tree(tree[1], role, replacement))
	else:
		if (tree[1] == role):
			tree = ('ROLE', replacement)
	return tree

#
# End helper functions for role counter
#

# counts the number of users who meet the provided boolean expression
@bot.command()
async def count(ctx, *role_str: str):
	role_str = " ".join(role_str)
	if role_str.lower() == "none":
		count = 0
		for member in ctx.guild.members:
			if len(member.roles) == 1:
				count += 1
		await ctx.send(str(count) + (" user has" if count == 1 else " users have") + " no role.")
	elif role_str.lower() == "all":
		count = 0
		for member in ctx.guild.members:
			count += 1
		await ctx.send(str(count) + (" user is" if count == 1 else " users are") + " in this server.")
	else:
		try:
			tokens = lexer(role_str)
			tree = parse(tokens)
		except:
			await ctx.send("Boolean expression '" + role_str.replace('@', '[at]') + "' could not be parsed.")
			return

		all_roles = ctx.guild.roles
		for role_name in get_roles_in_tree(tree):
			# if getting the role doesn't work, check to see if the uppercase version exists
			# people frequently try to count the number of majors but pass the lowercase version of the role
			role = get(all_roles, name=role_name) or get(all_roles, name=role_name.upper())
			if role is None:
				await ctx.send("No such role " + role_name.replace('@', '[at]') + ".")
				return
			if (role_name != role.name):
				tree = replace_roles_in_tree(tree, role_name, role.name)

		count = 0
		for member in ctx.guild.members:
			if evaluate(tree, member.roles):
				count += 1

		await ctx.send(str(count) + (' user meets the criteria for \'' if count == 1 else ' users meet the criteria for \'') + role_str.replace('@', '[at]') + '\'.')

# as described
@bot.command()
async def about(ctx):
	await ctx.send("Testudo: A bot for the UMD discord server.\nSource code available at https://gitlab.com/nahkoots/umd-discord-bot.")

# generates an embed containing data about a provided course
@bot.command()
async def course(ctx, *message: str):
	message = (" ".join(message)).replace(" ", "") # in case people use "course cmsc 216" or equivalent
	r = requests.get(courseEndpoint + message).json()
	if not 'error_code' in r:
		r = r[0]
		em = discord.Embed(
				title=r['course_id'] + ": " + r['name'],
				color=discord.Color.from_rgb(226, 24, 51),
				url=testudoUrl + r['semester']+"/"+r['dept_id']+"/"+r['course_id'])
		em.add_field(name="Description", value=r['description'] or "No description provided")
		em.add_field(name="Department", value=r['department'], inline=False)
		em.add_field(name="Prerequisites", value=r['relationships']['prereqs'] or "None", inline=False)
		em.add_field(name="Course ID", value=r['course_id'], inline=True)
		em.add_field(name="Credits", value=r['credits'], inline=True)
		await ctx.send(embed=em)
	elif r['error_code'] == 400:
		await ctx.send("Invalid format: course IDs take the form [major code][course number] (for example: CMSC132).")
	elif r['error_code'] == 404:
		await ctx.send("No such course.")

# expands a department's abbreviation
@bot.command()
async def department(ctx, *message: str):
	message = (" ".join(message)).upper()
	r = requests.get(departmentEndpoint).json()
	r = {i['dept_id']: i['dept_id'] + ": " + i['department'] for i in r}
	await ctx.send(r.get(message, "No such department."))

# allows a user to opt out of the politics chat
@bot.command()
async def apolitical(ctx):
	role = get(ctx.guild.roles, name = 'apolitical')
	if role is None:
		await ctx.send("No politics channel to opt out of.")
	if role in ctx.author.roles:
		await ctx.author.remove_roles(role)
		await ctx.send("Opted you back into the politics chat.")
	else:
		await ctx.author.add_roles(role)
		await ctx.send("Opted you out of the politics chat.")

# creates meme
@bot.command()
async def meme(ctx, *message: str):
	# returns message if no arguments provided; returns list of custom memes if "list" is sole argument; checks if image extension is attached
	hasImageExt = False
	if len(message) == 0:
		await ctx.send("meme [\"top text\" \"bottom text\" | <link> \"top text\" \"bottom text\" | <name> \"text 1\"...\"text n\" | list]")
		return
	elif ' '.join(message).lower() == 'list':
		msg = ""
		for meme in custom_memes.keys():
			msg += meme + ": " + str(len(custom_memes[meme]['texts'])) + " arguments\n"
		msg = msg[:-1] # delete last newline
		await ctx.send(msg)
		return
	else:
		extensions = ['png', 'jpg', 'jpeg']
		for e in extensions:
			if "." + e == message[0][(-len(e)-1):]:
				hasImageExt = True

	# use standard meme format if an image or url is attached, else use custom meme formats
	if ctx.message.attachments or hasImageExt:
		# get image from url or attachment and create background layer
		try:
			url = ctx.message.attachments[0].url if ctx.message.attachments else message[0]
			message = message if ctx.message.attachments else message[1:]
			response = requests.get(url)
			base = Image.open(BytesIO(response.content)).convert("RGBA")
		except:
			await ctx.send("Unable to open image")
			return

		# create text layer
		txt = Image.new("RGBA", base.size, (255,255,255,0))
		d = ImageDraw.Draw(txt)

		# set text formatting
		base_width, base_height = base.size
		font_size = int(base_height / 10)
		font = ImageFont.truetype("meme/impact.ttf", font_size)
		fill = (255,255,255)
		stroke_width = int(font_size / 10)
		stroke_fill = (0,0,0)

		# get text and dimensions
		text0 = message[0].upper() if len(message) > 0 else ""
		text1 = message[1].upper() if len(message) > 1 else ""
		w0, h0 = d.textsize(text0, font=font)
		w1, h1 = d.textsize(text1, font=font)

		# find maximum font size without overflowing text
		while (w0 > base_width - int(base_width/20) or w1 > base_width - int(base_width/20)) and font_size >= 0:
			font_size -= 1
			font = ImageFont.truetype("meme/impact.ttf", font_size)
			stroke_width = int(font_size / 10)
			w0, h0 = d.textsize(text0, font=font)
			w1, h1 = d.textsize(text1, font=font)

		# add text to layer
		d.text(((base_width-w0)/2,int(base_width/100)), text0, font=font, fill=fill, stroke_width=stroke_width, stroke_fill=stroke_fill)
		d.text(((base_width-w1)/2,base_height-int(base_width/100)-h1-2*stroke_width), text1, font=font, fill=fill, stroke_width=stroke_width, stroke_fill=stroke_fill)

		# combines layers and sends image
		out = Image.alpha_composite(base, txt)
		with BytesIO() as image_binary:
			out.save(image_binary, 'PNG')
			image_binary.seek(0)
			try:
				await ctx.send(file=discord.File(fp=image_binary, filename='image.png'))
				await ctx.message.delete()
			except:
				await ctx.send("Image too large")
	else:
		# get meme settings
		try:
			meme = custom_memes[message[0]]
		except:
			await ctx.send("Meme not found")
			return

		# get image and create image and text layers
		base = Image.open("meme/" + meme['filename']).convert("RGBA")
		txt = Image.new("RGBA", base.size, (255,255,255,0))
		d = ImageDraw.Draw(txt)

		# set text formatting
		font = ImageFont.truetype("meme/impact.ttf", meme['font_size'])
		fill = (255,255,255)
		stroke_width = int(meme['font_size'] / 10)
		stroke_fill = (0,0,0)

		# iterates through all defined possible texts
		for i in range(len(meme['texts'])):
			j, row = 0, 0
			text_settings = meme['texts'][i]
			words = message[i+1].split(' ') if len(message) > i+1 else []

			# splits words into rows
			while j < len(words):
				text = ""
				w, h = d.textsize(text, font=font)

				# gets all rows up until first word to fully exceed max width
				while w <= text_settings['max_width'] and j < len(words):
					text += " " + words[j].upper()
					text = text.strip()
					w, h = d.textsize(text, font=font)
					j += 1

				# adds current row to layer
				d.text((text_settings['center_x']-w/2,text_settings['top_y']+h*row), text, font=font, fill=fill, stroke_width=stroke_width, stroke_fill=stroke_fill)
				row += 1

		# combines layers and sends image
		out = Image.alpha_composite(base, txt)
		with BytesIO() as image_binary:
			out.save(image_binary, 'PNG')
			image_binary.seek(0)
			try:
				await ctx.send(file=discord.File(fp=image_binary, filename='image.png'))
			except:
				await ctx.send("Image too large")

# combines flashtudo with logo
@bot.command()
async def testudo(ctx, *link: str):
	extensions = ['png', 'jpg', 'jpeg']
	if ctx.message.attachments or (len(link) != 0 and any(["." + e == link[0][(-len(e)-1):] for e in extensions])):
		try:
			url = ctx.message.attachments[0].url if ctx.message.attachments else link[0]
			response = requests.get(url)
			logo = Image.open(BytesIO(response.content)).convert("RGBA")
		except:
			await ctx.send("Unable to open image")
			return

		logo = logo.resize((512, 512))
		bottom = Image.open("meme/testudobottom.png").convert("RGBA")
		top = Image.open("meme/testudotop.png").convert("RGBA")

		bottom.paste(logo, (293, 330), logo)
		bottom.paste(top, (0,0), top)
		with BytesIO() as image_binary:
			bottom.save(image_binary, 'PNG')
			image_binary.seek(0)
			try:
				await ctx.send(file=discord.File(fp=image_binary, filename='image.png'))
				await ctx.message.delete()
			except:
				await ctx.send("Image too large")
	else:
		await ctx.send("testudo [<link>] (or image attachment)")

#
#	Below this point: administrative commands
#

# as described
@bot.command()
async def adminhelp(ctx):
	await ctx.send("Testudo administrative help menu.\n" +
			"voice: For use in community servers. Assigns the default role (\".\") to users who have no other roles.\n" +
			"backup: Saves each user's list of roles to a file.\n" +
			"populate: Creates all of the major and year roles for the server.\n" +
			"depopulate: Deletes all of the major and year roles.\n" +
			"delrole [role]: Deletes all copies of the provided role.\n" +
			"restrict: Removes some default permissions from all major and year roles.")

# assigns the default role to users who need it and do not already have it
@bot.command()
@commands.check_any(commands.has_permissions(manage_roles=True), authorized())
async def voice(ctx):
	role = get(ctx.guild.roles, name = settings['speechrole'])
	for member in ctx.guild.members:
		if len(member.roles) == 1:
			await member.add_roles(role)
	await ctx.send("Done.")

# iterates over users and saves all of their roles to a file
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def backup(ctx):
	userroles = {}
	for member in ctx.guild.members:
		roles = [role.name for role in member.roles]
		userroles[member.id] = roles
	with open('backups/' + ctx.guild.name, 'a+') as backup:
		json.dump(userroles, backup)
	await ctx.send("Done.")

# creates any of the major and year roles for the server that do not already exist
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def populate(ctx):
	for role_str in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role_str)
		if role is None:
			await ctx.guild.create_role(name=role_str)
	await ctx.send("Done.")

# deletes all of the major and year roles for the server
# use with caution
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def depopulate(ctx, confirm: str=None):
	if (confirm != "confirm"):
		await ctx.send("Type " + settings['prefix'] + "depopulate confirm to run this command.")
		return
	for role in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role)
		if role is not None:
			await role.delete()
	await ctx.send("Done.")

# deletes all instances of a role
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def delrole(ctx, *role_str: str):
	role_str = " ".join(role_str)
	role = get(ctx.guild.roles, name=role_str)
	while role is not None:
		await role.delete()
		role = get(ctx.guild.roles, name=role_str)
	await ctx.send("Done.")

# removes some of the permissions from each year/major role
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def restrict(ctx):
	for role in settings['majors'] + settings['years']:
		role = get(ctx.guild.roles, name=role)
		if role is not None:
			p = role.permissions
			p.send_tts_messages = False
			p.embed_links = False
			p.attach_files = False
			await role.edit(permissions=p)
	await ctx.send("Done.")

# repeats the last message sans command
@bot.command()
@authorized()
async def say(ctx, *message: str):
	message = " ".join(message)
	await ctx.send(message)
	await ctx.message.delete()

#
# Begin robot 9000 algorithm
# https://blog.xkcd.com/2008/01/14/robot9000-and-xkcd-signal-attacking-noise-in-chat/
#
@bot.command()
@commands.check_any(commands.has_permissions(administrator=True), authorized())
async def index(ctx):
	c = conn.cursor()
	# start from a fresh db on every reindex
	c.execute("DELETE FROM MESSAGE")
	# make sure to commit before starting a new transaction
	conn.commit()
	# insert messages inside a transaction for performance
	c.execute('BEGIN TRANSACTION')
	for channel in ctx.guild.channels:
		if channel.type is not discord.ChannelType.text:
			continue
		async for message in channel.history(limit=None):
			r9k_insert_message(message)
	conn.commit()

def db_member_id(user_id):
	ret = conn.execute("SELECT id FROM MEMBER WHERE user_id=?", [user_id]).fetchone()
	# return null if no member by that id exists
	return ret[0] if ret else None

def r9k_insert_message(message):
	# XXX: caller is responsible for committing this execution.
	c = conn.cursor()
	if not db_member_id(message.author.id):
		c.execute("INSERT INTO MEMBER (user_id) VALUES (?)", [message.author.id])
	member_id = db_member_id(message.author.id)
	content = r9k_clean_message(message)
	original_text = message.content
	c.execute("INSERT INTO MESSAGE (member_id, message_id, content, original_text) VALUES (?, ?, ?, ?)", [member_id, message.id, content, original_text])

async def r9k_process_message(message):
	if not settings["r9k_channel_id"] or message.channel.id != settings["r9k_channel_id"]:
		return

	c = conn.cursor()
	content = r9k_clean_message(message)
	# ignore case when comparing messages
	if not c.execute("SELECT * FROM MESSAGE where LOWER(content) = LOWER(?)", [content]).fetchone():
		return

	member_id = db_member_id(message.author.id)
	last_mute = c.execute("SELECT start, duration FROM MUTE where member_id = ? ORDER BY datetime(start) DESC LIMIT 1", [member_id]).fetchone()
	# user has never been muted before from r9k
	if not last_mute:
		duration = settings["r9k_initial_mute_duration"]
		r9k_channel = f"<#{settings['r9k_channel_id']}>"
		dm_message = (
			f"Because you sent a message {r9k_channel} which is identical to other messages sent in the server before, you "
			f"have been prevented from sending messages in {r9k_channel} for another {duration} seconds. Be aware that your "
			"mute duration quadruples on every offense and decays by half every 6 hours."
			"\n\n"
			"This is part of our server's ROBOT9000 implementation, an approach for reducing noise originally described by "
			"Randall Monroe: https://blog.xkcd.com/2008/01/14/robot9000-and-xkcd-signal-attacking-noise-in-chat/"
		)
	else:
		(last_start, last_duration) = last_mute
		decay_hours = (datetime.utcnow() - last_start).total_seconds() / 3600
		# duration decays by half every 6 hours
		duration = last_duration / (2 ** (decay_hours / 6))
		# duration quadruples every time (calculated after decay)
		duration *= 4
		# don't allow durations to decay below the initial duration
		duration = max(duration, settings["r9k_initial_mute_duration"])
		dm_message = f"You have been muted for {int(duration)} seconds in <#{settings['r9k_channel_id']}> as part of our ROBOT9000 implementation."

	await message.delete()
	role = message.guild.get_role(settings["r9k_unoriginal_role_id"])
	await message.author.add_roles(role)

	start = datetime.utcnow()
	c.execute("INSERT INTO MUTE (member_id, start, duration) VALUES (?, ?, ?)", [member_id, start, duration])
	channel = await message.author.create_dm()
	await channel.send(dm_message)

	conn.commit()

@tasks.loop(seconds=1)
async def check_expired_mutes():
	# get all mutes which have expired
	mutes = conn.execute(
		"""
		SELECT * FROM MUTE WHERE
		datetime(strftime('%s', substr(start, 1, 19)) + duration, 'unixepoch') < datetime('now')
		AND NOT applied
		"""
	).fetchall()

	for mute in mutes:
		mute_id = mute[0]
		member_id = mute[1]
		user_id = conn.execute("SELECT user_id FROM MEMBER WHERE id=?", [member_id]).fetchone()[0]
		guild = bot.get_guild(settings["guild_id"])
		role = guild.get_role(settings["r9k_unoriginal_role_id"])
		await guild.get_member(user_id).remove_roles(role)
		conn.execute("UPDATE MUTE SET applied = 1 WHERE id = ?", [mute_id])
		conn.commit()

@check_expired_mutes.before_loop
async def before_check_expired_mutes():
	# make sure the bot has started up before trying to run the loop
	await bot.wait_until_ready()

check_expired_mutes.start()

bot.run(json.load(open('auth.json'))['token'])
